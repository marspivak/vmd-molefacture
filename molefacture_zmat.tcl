##
## $Id: molefacture_gui.tcl,v 1.75 2020/08/13 21:48:37 mariano Exp $
##

###############################################################################
# NEW Molefacture Version = 1.5 (major gui overhaul)
# 
# The new interface is based on two main areas/panels:
#   The left area where different modes can be implemented, by changing this 
#   panel to a notebook and adding tabs to it. As for the first 
#   implementation for the first version, only a table (and no notebook)
#   with the atoms fields was implemented. One can imagine using this left panel 
#   to extend the functionality of molefacture like add QM input file generator.
#
#   the right panel contains all the buttons and action related widgets, like
#   slide bars and modifiers that affect the molecule.
#
#
#   Many of the functionality previously available in the Molefacture were 
#   disabled to make sure that the modeling part of the tool worked properly
#   slowly these functionalities should be re-implemented.
#
###############################################################################
#
######################################################
# Window to use Z-matrix in Molefacture              #
######################################################
proc ::Molefacture::zmatWindows { {frame ""} } {
  global env
  variable topGui
  variable zmatWindow

   if {$frame != ""} {
      set zmatWindow $frame
   } else {
      if { [winfo exists $zmatWindow] ==1} {
         wm deiconify $zmatWindow
         focus $zmatWindow
         raise $zmatWindow
         return
      }
      set w [toplevel $zmatWindow]
      wm title $w "Z-matrix editor"
      wm resizable $w 1 1
   }

   grid columnconfigure $zmatWindow 0 -weight 1
   grid rowconfigure $zmatWindow 1 -weight 1

   variable atomsel

   set atomsel ""
   
   set mainrow 0
   ############## Frame for Atom Table List and Editing #################

   set tbframe "$zmatWindow.tbframe"
   grid [ttk::frame $tbframe] -row 0 -column 0 -sticky nswe
   grid columnconfigure $tbframe 0 -weight 1
   grid rowconfigure $tbframe 0 -weight 1

   option add *Tablelist.activeStyle       frame

   option add *Tablelist.movableColumns    no
   option add *Tablelist.labelCommand      tablelist::sortByColumn

   tablelist::tablelist $tbframe.tb \
        -columns { 0 "Index" center
                0 "Atom Name"    center
                0 "A" center
                0 "Bond" center
                0 "B" center
                0 "Angle" center
                0 "C" center
                0 "Dihedral" center
                } \
                -yscrollcommand [list $tbframe.scr1 set] -xscrollcommand [list $tbframe.scr2 set] \
                -showseparators 0 -labelrelief groove -labelcommand tablelist::sortByColumn  -labelbd 1 -selectforeground black\
                -foreground black -background white -state normal -width 50 -stretch "all" -editselectedonly true -selectmode extended\
                -stripebackgroun white -exportselection true -height 40 \
                -editstartcommand Molefacture::startEditZMatTable -editendcommand Molefacture::validateEditZMatTable -forceeditendcommand false

   $tbframe.tb columnconfigure 0 -selectbackground cyan -width 0 -maxwidth 0 -name Index -sortmode integer
   $tbframe.tb columnconfigure 1 -selectbackground cyan -width 0 -maxwidth 0 -editable true -editwindow ttk::entry -name AtmNAME -sortmode dictionary
   $tbframe.tb columnconfigure 2 -selectbackground cyan -width 0 -maxwidth 0 -editable true -editwindow ttk::entry -name Atm1 -sortmode dictionary
   $tbframe.tb columnconfigure 3 -selectbackground cyan -width 0 -maxwidth 0 -editable true -editwindow ttk::entry -name ZBond -sortmode dictionary
   $tbframe.tb columnconfigure 4 -selectbackground cyan -width 0 -maxwidth 0 -editable true -editwindow ttk::entry -name Atm2 -sortmode dictionary
   $tbframe.tb columnconfigure 5 -selectbackground cyan -width 0 -maxwidth 0 -editable true -editwindow ttk::entry -name ZAngle -sortmode dictionary
   $tbframe.tb columnconfigure 6 -selectbackground cyan -width 0 -maxwidth 0 -editable true -editwindow ttk::entry -name Atm3 -sortmode dictionary
   $tbframe.tb columnconfigure 7 -selectbackground cyan -width 0 -maxwidth 0 -editable true -editwindow ttk::entry -name ZDihedral -sortmode dictionary

   grid $tbframe.tb -row 0 -column 0 -sticky news
   grid columnconfigure $tbframe.tb 0 -weight 1; grid rowconfigure $tbframe.tb 0 -weight 1

   ## Scroll Bar Vertical
   scrollbar $tbframe.scr1 -orient vertical -command [list $tbframe.tb  yview]
   grid $tbframe.scr1 -row 0 -column 1  -sticky ens

   ## Scroll Bar Horizontal
   scrollbar $tbframe.scr2 -orient horizontal -command [list $tbframe.tb xview]
   grid $tbframe.scr2 -row 1 -column 0 -sticky swe

   set Molefacture::zmatTable $tbframe.tb

   ::Molefacture::zmatUpdate

   #bind $tbframe.tb <<TablelistSelect>> { if {[$Molefacture::zmatTable size] > 0} {::Molefacture::selectAtomTable} }

   #bind $tbframe.tb <<TablelistSelectionLost>> Molefacture::bindAtomTable

}

# Idea, separate the finding of bond,angle,dihedralpartner values

proc ::Molefacture::zmatPartners { {force 0} {type A} {atom 0} {newval 0} } {
   variable zmat
   set sel [atomselect $Molefacture::tmpmolid "$::Molefacture::notDumSel"]
   
   # I want to populate the table with a zmat scheme
   # define bonds: 1) find bonds (topo) containing the index i
   if {[topo getbondlist] == ""} {
      topo guessbonds
   }
   set auxbondlist [topo getbondlist]
   set indexlist [$sel get index]
   
   if {!$force} {
      # first step, empty lists
      set zmat(bondpartner) {1}; #
      set zmat(anglepartner) {0 0}
      set zmat(dihedralpartner) {0 0 0}

      #find bonds, angle, dihedrals

      for {set i 1} {$i < [llength $indexlist]} {incr i} {
         set istart [ expr $i - 1 ]
         # idea: find index j to whom index i is bond. starting from index i-1
         
         # first find the topo bond that contains i
         foreach bond $auxbondlist {
            if {[regexp $i $bond]} {
               for {set j $istart} { $j > -1 } { incr j -1 } {
                  if {[regexp "$j " $bond]} {
                     # found j
                     set Atm1 $j
                     lappend zmat(bondpartner) $Atm1
                     break
                  }
               }
               break
            }
         }
         if {$i > 1} {
            # get index of Atm2, that forms a angle with i and Atm1
            # we only need to check Atm2 such that i > Atm2, going down
            for {set k $istart} { $k > -1 } { incr k -1 } {
               if {$k == [lindex $zmat(bondpartner) $j]} {
                  # Add check k is bond to j
                  # found k
                  set Atm2 $k
                  lappend zmat(anglepartner) $Atm2
                  break
               }
            }
         }
         if {$i > 2} {
            # we need the dihedral aswell
            # looking for dihedralpartner, which must be different from bondpartner and anglepartner
            # new idea: let's try first the bondpartner of my anglepartner, check that is not Atm1
            if {[lindex $zmat(bondpartner) $Atm2] != $Atm1} {
               set Atm3 [lindex $zmat(bondpartner) $Atm2]
               lappend zmat(dihedralpartner) $Atm3
            } else {
               # we chose the first index that is not Atm1 or Atm2
               # loop from i-1 to 0, skip if we are in Atm1 or Atm2
               for {set d $istart} { $d > -1 } { incr d -1 } {
                  if {$d != $Atm1 && $d != $Atm2} {
                     set Atm3 $d
                     lappend zmat(dihedralpartner) $Atm3
                     break
                  }
               }
               #$d == [lindex $zmat(bondpartner) [lindex $zmat(anglepartner) $i]]
            }
         }   
      }     


   } ;# end if bondpartner is empty

   # if user modifies partners A,B or C, we check they are not repeated
   if {$force} {
      switch $type {

         A {
            # user modified the bondpartner A
            # check the new A is =! than B
            if {$newval == [lindex $zmat(anglepartner) $atom]} {
               # replace B with a different index, first atempt is to use old A
               set zmat(anglepartner) [lreplace $zmat(anglepartner) $atom $atom [lindex $zmat(bondpartner) $atom]]
            # and check the new A is =! than C   
            } elseif {$newval == [lindex $zmat(dihedralpartner) $atom]} {
               # replace C with a different index
               set zmat(dihedralpartner) [lreplace $zmat(dihedralpartner) $atom $atom [lindex $zmat(bondpartner) $atom]]
            }
            set zmat(bondpartner) [lreplace $zmat(bondpartner) $atom $atom $newval]
         }

         B {
            # user modified the anglepartner B
            # check the new B is =! than A
            if {$newval == [lindex $zmat(bondpartner) $atom]} {
               # replace A with a different index, first atempt is to use old B
               set zmat(bondpartner) [lreplace $zmat(bondpartner) $atom $atom [lindex $zmat(anglepartner) $atom]]
            # and check the new B is =! than C   
            } elseif {$newval == [lindex $zmat(dihedralpartner) $atom]} {
               # replace C with a different index
               set zmat(dihedralpartner) [lreplace $zmat(dihedralpartner) $atom $atom [lindex $zmat(anglepartner) $atom]]
            }
            set zmat(anglepartner) [lreplace $zmat(anglepartner) $atom $atom $newval]
         }

         C {
            # user modified the dihedralpartner C
            # check the new C is =! than A
            if {$newval == [lindex $zmat(bondpartner) $atom]} {
               # replace A with a different index, first atempt is to use old C
               set zmat(bondpartner) [lreplace $zmat(bondpartner) $atom $atom [lindex $zmat(dihedralpartner) $atom]]
            # and check the new C is =! than B   
            } elseif {$newval == [lindex $zmat(anglepartner) $atom]} {
               # replace C with a different index
               set zmat(anglepartner) [lreplace $zmat(anglepartner) $atom $atom [lindex $zmat(dihedralpartner) $atom]]
            }
            set zmat(dihedralpartner) [lreplace $zmat(dihedralpartner) $atom $atom $newval]
         }

      }
   }

}

proc ::Molefacture::zmatUpdate {} {
   # fill zmat table based on partners A,B,C
   variable zmatTable
   variable zmat

   if {[array names ::Molefacture::zmat] == ""} {
      ::Molefacture::zmatPartners
   }

   # fill zmat table with values from built molecule
   
   set sel [atomselect $Molefacture::tmpmolid "$::Molefacture::notDumSel"]
   set names [$sel get name]
   set indexlist [$sel get index]

   set tmplist {}
   
   $zmatTable delete 0 end

   lappend tmplist "0 [lindex [$sel get name] 0] - - - - - -"
   lappend tmplist [format "%i %s %i %4.3f %s %s %s %s" "1" [lindex $names 1] [lindex $zmat(bondpartner) 1] [measure bond \
   [list 1 [lindex $zmat(bondpartner) 1]]] "-" "-" "-" "-"]
   lappend tmplist [format "%i %s %i %4.3f %i %4.2f %s %s" "2" [lindex $names 2] [lindex $zmat(bondpartner) 2] [measure bond \
   [list 2 [lindex $zmat(bondpartner) 2]]] [lindex $zmat(anglepartner) 2] [measure angle [list 2 [lindex $zmat(bondpartner) 2] [lindex $zmat(anglepartner) 2]]] "-" "-"]

   for {set i 3} {$i < [llength $indexlist]} {incr i} {
      set Atm1 [lindex $zmat(bondpartner) $i]
      set Atm2 [lindex $zmat(anglepartner) $i]
      set Atm3 [lindex $zmat(dihedralpartner) $i]
      lappend tmplist [format "%i %s %i %4.3f %i %4.2f %i %4.2f" $i [lindex $names $i] $Atm1 [measure bond \
      [list $i $Atm1]] $Atm2 [measure angle [list $i $Atm1 $Atm2]] $Atm3 [measure dihed [list $i $Atm1 $Atm2 $Atm3]]]
   }   
      
   # need to populate the table
   for {set i 0} {$i < [llength $indexlist]} {incr i} {
      #puts [lindex $tmplist $i]
      $zmatTable insert end [lindex $tmplist $i]
   }

}   

#######################################################
# Start of the edit process of the Z-matrix Table       #
#######################################################
proc Molefacture::startEditZMatTable {tbl row col txt} {
   set txt [string trim $txt]
   set tblsel [$tbl curselection]
   set indexlist ""
   set w [$tbl editwinpath]
   set valrow [$tbl get $tblsel]

   set atmTab $::Molefacture::atomTable

   foreach row $tblsel {
      lappend indexlist [$tbl cellcget $row,Index -text]
   }
   if {[llength $indexlist] == 0} {
      return
   }
   if {[$tbl canceledediting] == 1 } {
      return
   }
   ### Column 1 - Atom's name
   switch [$tbl columncget $col -name] {

      ZBond {
         puts "This is a bond between $row and [lindex $valrow 2]"
         puts "updating the atomTable with new selection"
         $atmTab selection clear 0 end
         $atmTab selection set $row 
         $atmTab selection set [lindex $valrow 2]
         puts "Calling selectAtomTable proc to update visual selections"
         ::Molefacture::selectAtomTable

      }

      ZAngle {
         puts "This is an angle between $row, [lindex $valrow 2] and [lindex $valrow 4]"
         $atmTab selection clear 0 end
         $atmTab selection set $row 
         $atmTab selection set [lindex $valrow 2]
         $atmTab selection set [lindex $valrow 4]
         ::Molefacture::selectAtomTable

      }

      ZDihedral {
         puts "This is a dihedral between [lindex $valrow 0], [lindex $valrow 2], [lindex $valrow 4] and [lindex $valrow 6]"
         $atmTab selection clear 0 end
         $atmTab selection set $row 
         $atmTab selection set [lindex $valrow 2] 
         $atmTab selection set [lindex $valrow 4] 
         $atmTab selection set [lindex $valrow 6]
         ::Molefacture::selectAtomTable

      }

   }

   return $txt
}

#######################################################
# Validation of the edit process of the Atom's Table  #
#######################################################
proc Molefacture::validateEditZMatTable {tbl row col txt} {
   # set prevtext [$tbl cellcget $row,$col -text]
   set txt [string trim $txt]
   set tblsel [$tbl get $row]
   set valrow [$tbl get $row]
   puts $valrow
   set indexlist ""
   set tableindexlist ""

   set atmTab $::Molefacture::atomTable

   puts "Inside validate $tbl $row $col $txt"

   foreach row $tblsel {
      #lappend indexlist [$tbl cellcget $row,Index -text]
   }
   if {[llength $indexlist] == 0} {
      #return
   }
   if {[$tbl canceledediting] == 1 } {
      ::Molefacture::clearTableSelection
      return
   }
   ### Column 1 - Atom's name
   switch [$tbl columncget $col -name] {

      ZBond {
         puts "Changing bond value between [lindex $valrow 0] and [lindex $valrow 2]: $txt"
         set Molefacture::bondguiValentry $txt
         ::Molefacture::validateBondEntry
         ::Molefacture::saveState
         ::Molefacture::adjust_bondlength $Molefacture::bondguiValentry
         #::Molefacture::zmatUpdate
      }

      ZAngle {
         puts "This is an angle between [lindex $valrow 0], [lindex $valrow 2] and [lindex $valrow 4]"
         set Molefacture::angleguiValentry $txt
         Molefacture::validateAngleEntry
         Molefacture::saveState
         set Molefacture::anglemvgrp2 1
         ::Molefacture::resize_angle $Molefacture::angleguiValentry

      }

      ZDihedral {
         puts "This is a dihedral between [lindex $valrow 0], [lindex $valrow 2], [lindex $valrow 4] and [lindex $valrow 6]"
         set Molefacture::dihguiValentry $txt
         Molefacture::validateAngleEntry
         Molefacture::saveState
         set Molefacture::dihedrgrp group2
         ::Molefacture::rotate_bond $Molefacture::dihguiValentry

      }

      Atm1 {
         puts "Changing A"
         ::Molefacture::zmatPartners 1 A [lindex $valrow 0] $txt
         ::Molefacture::zmatUpdate
      }

      Atm2 {
         puts "Changing B"
         ::Molefacture::zmatPartners 1 B [lindex $valrow 0] $txt
         ::Molefacture::zmatUpdate
      }

      Atm3 {
         puts "Changing C"
         ::Molefacture::zmatPartners 1 C [lindex $valrow 0] $txt
         ::Molefacture::zmatUpdate
      }

   }
   mol reanalyze $Molefacture::tmpmolid
   if {$Molefacture::topocgenff == 1} {
      set Molefacture::topocgenff 0
   }

   ::Molefacture::clearTableSelection

   return $txt
}
